from __future__ import annotations
import os
import time
import msgpack
from socketserver import ThreadingMixIn, UDPServer, TCPServer, BaseRequestHandler, socket
from threading import Thread, Lock
from models.param_code import ParamCode
from models.operation_data import OperationData
from handlers.auth_handler import AuthHandler
from handlers.move_handler import MoveHandler
    

class ServerHandler(BaseRequestHandler):

    def handle(self):
        data = self.request[0]
        sock: socket = self.request[1]
        try:
            opData = msgpack.unpackb(data)
            print(opData)
            self.data = OperationData.deserialize(opData)
            try: 
                code = self.data.code
                self.handleOperation(code, sock)
            except KeyError:
                sock.sendto("operation code not found",self.client_address)
        except KeyError:
            sock.sendto("data is not valid",self.client_address)
        except ValueError:
            sock.sendto("Message is not a valid operation data",self.client_address)

    def handleOperation(self, code, socket):
        if Server.instance().is_running is False: 
            raise Exception("Server not Running")
        print(code)
        if ParamCode(code) >= ParamCode.READY_START:
            raise Exception("Param Code not responsable for this")
        return {
            ParamCode.AUTH: Server.instance().handle_auth.handle(self.data.data, self.client_address),
            ParamCode.MOVE: Server.instance().handle_move.handle(self.data.data,self.client_address)
            # ParamCode.MOVE: ApplicationSession.instance().handle_move.handle(self.data)
        }[code]


class Server(ThreadingMixIn, UDPServer):
    __instance = None 
    def __init__(self, server_address, handler=ServerHandler):
        super(Server, self).__init__(server_address,handler)
        self.server_thread = None
        self.is_running = False
        self.password = "12345"
        self.connectedClients = list()
        self.handle_auth = AuthHandler()
        self.handle_move = MoveHandler()

    def serve(self):
        self.server_thread = Thread(target=self.serve_forever)
        self.server_thread.daemon = True
        try:
            self.server_thread.start()
            self.is_running = True
            print("Server started at {} port {}".format(self.server_address[0],self.server_address[1]))
            self.loop()
        except (KeyboardInterrupt, SystemExit):
            self.shutdown()
            self.server_close()
            exit()
    def loop(self):
        while True:
            time.sleep(1)
    @classmethod
    def instance(cls) -> 'Server': 
        if cls.__instance is None:
            cls.__instance = Server((os.getenv('HOST'),int(os.getenv('PORT'))))
        return cls.__instance