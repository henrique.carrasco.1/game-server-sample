from io import BytesIO
import msgpack
from models.response_data import ResponseData
from handlers.base_application_handler import BaseApplicationHandler
from models.player import Player
from models.vector2 import Vector2
from models.param_code import ResponseCode
import controllers.player_controller as PlayerController
class AuthHandler(BaseApplicationHandler):
    def handle(self, data, client_addr=None) -> bytearray:
        from core.server import Server
        if data['auth'] == Server.instance().password:
            if(client_addr not in Server.instance().connectedClients):
                player = PlayerController.instance.createPlayer()
                Server.instance().connectedClients.append([client_addr,player])
            response = ResponseData(0,"OK")
            buffer = BytesIO()
            msgpack.pack(response,buffer, default=ResponseData.serialize)
            buffer.seek(0)
            buffer = bytearray(buffer.getvalue())
            Server.instance().socket.sendto(buffer,client_addr)
            for clients in Server.instance().connectedClients:
                buffer = BytesIO()
                response = ResponseData(ResponseCode.CREATE_PLAYER, player)
                msgpack.pack(response,buffer, default=ResponseData.serialize)
                buffer.seek(0)
                buffer = bytearray(buffer.getvalue())
                Server.instance().socket.sendto(buffer,clients[0])
            return buffer
