from abc import ABC, abstractmethod
class BaseApplicationHandler(ABC):
    @abstractmethod
    def handle(self, data, client_addr: (str, int) = None) -> bytearray:
        pass