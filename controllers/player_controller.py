from models.player import Player
from models.vector2 import Vector2

class PlayerController: 
    def createPlayer(self, customColor="34ebe8"):
        from core.server import Server
        return Player(len(Server.instance().connectedClients)+1,Vector2.zero(),0.5,customColor)

    def move(self, player: Player, axis):
       return player.position + axis



instance = PlayerController()