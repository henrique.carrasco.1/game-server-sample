from __future__ import annotations
from models.param_code import ParamCode
from models.serializable_object import SerializableObject

class OperationData(SerializableObject):
    def __init__(self, code: ParamCode, data: object):
        self.code = code
        self.data = data
    @staticmethod
    def serialize(obj: 'OperationData'):
        data = obj.data
        if isinstance(data,SerializableObject):
            data = obj.data.__class__.serialize(obj.data)
        return [obj.code, data]
    @staticmethod
    def deserialize(obj):
        return OperationData(obj[0],obj[1])

    # def getData(self, type):
        # return self.data;    

