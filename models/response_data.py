from __future__ import annotations
from models.param_code import ResponseCode
from models.serializable_object import SerializableObject


class ResponseData(SerializableObject):
    def __init__(self, code: ResponseCode, data: object):
        self.code = code
        self.data = data
    @staticmethod
    def serialize(obj: 'ResponseData'):
        data = obj.data
        if isinstance(data,SerializableObject):
            data = obj.data.__class__.serialize(obj.data)
        return [obj.code, data]
    @staticmethod
    def deserialize(obj):
        return ResponseData(obj[0],obj[1])