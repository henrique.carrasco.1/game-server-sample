from abc import ABC, abstractstaticmethod
class SerializableObject(ABC): 
    @abstractstaticmethod
    def serialize(obj):
        pass
    @abstractstaticmethod
    def deserialize(obj):
        pass