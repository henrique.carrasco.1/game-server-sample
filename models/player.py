from models.serializable_object import SerializableObject
from models.vector2 import Vector2
class Player(SerializableObject): 
    def __init__(self, viewId: int, position: Vector2, scale: int, color: str):
        self.viewId = viewId
        self.position = position
        self.scale = scale
        self.color = color

    @staticmethod
    def serialize(obj):
        print(obj)
        return [obj.viewId,[obj.position.x,obj.position.y],obj.scale,obj.color]
    @staticmethod
    def deserialize(obj):
        try: 
            return Player(obj[0],Vector2(obj[1][0],obj[1][1]),obj[2],obj[3])
        except KeyError:
            print("couldn't deserialize player object")
        except ValueError:
            print("couldn't deserialize player object")