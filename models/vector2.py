import json
class Vector2(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, b):
        return Vector2(self.x + b.x, self.y + b.y)
    def __mul__(self, b):
        if isinstance(b, Vector2):
            return Vector2(self.x * b.x, self.y * b.y)
        else:
            return Vector2(self.x * b, self.y * b)
    def __sub__(self,b):
        return Vector2(self.x - b.x, self.y - b.y)

    def toJson(self):
        return json.dumps(self)
    
    @staticmethod
    def fromJson(_json):
        obj = json.load(_json)
        return Vector2(obj['x'],obj['y'])
    
    @staticmethod
    def zero(): return Vector2(0,0)
    
    @staticmethod
    def up(): return Vector2(0,1)
    
    @staticmethod
    def right (): return Vector2(1,0)
    
    