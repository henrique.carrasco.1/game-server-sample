from enum import IntEnum

class ParamCode(IntEnum):
    AUTH = 0
    # tcp < 120, udp >= 120
    READY_START = 120
    MOVE = 121
    EAT = 122

class ResponseCode(IntEnum):
    AUTH_OK = 0
    CREATE_PLAYER = 121
    