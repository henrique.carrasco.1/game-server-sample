from dotenv import load_dotenv
load_dotenv()
from core.server import Server
if __name__ == "__main__":
    Server.instance().serve()


